<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\TodoResource;
use App\Todo;
use Illuminate\Http\Request;
use League\CommonMark\Extension\SmartPunct\Quote;

class TodoController extends Controller
{
    public function index()
    {
        $todos = Todo::latest()->paginate(10);

        return TodoResource::collection($todos);
    }
    public function store(Request $request)
    {
        $this->validate($request, [
            'todo' => 'required',
         // 'user_id' => 'required|user_id'
        ]);

        $todo = Todo::create([
            'user_id' => auth()->id(),
            'todo' => $request->todo,
        ]);

        return new TodoResource($todo);
    }
}
