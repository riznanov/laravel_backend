# Demo Laravel Backend with Authorization

### Auth
| Endpoint  | HTTP | Description     | Body                            |
| --------- | ---- | --------------- | ------------------------------- |
| `/me`     | GET  | Get profile user | `email` , `password` |
| `/todo`   | POST | Create New Todo      | `todo`          |

### Register
| Endpoint     | HTTP   | Description           | Body                                |
| ------------ | ------ | --------------------- | ----------------------------------- |
| `/register`    | POST    | Create New User          |`name`,`email`,`password`                                     |

### Login

| Endpoint          | HTTP   | Description            | Body               |
| ----------------- | ------ | ---------------------- | ------------------ |
| `/login`          | POST   | Login User              | `email`, `password` |

### Todo
| Endpoint                | HTTP   | Description                | Body   |
| ----------------------- | ------ | -------------------------- | ------ |
| `/todo`                | POST   | Create New Todo with api token | `todo` |
| `/todo`                | GET    | Get All Todos               |        |
